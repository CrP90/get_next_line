This project is the final starting project before branching out at 42.

The goal is to make a function which reads a single line from a file descriptor,
assuming it isn't being tampered with in between calls to the function.

Usage:

make -C libft/

./run.sh

./test_gnl /test/m.txt

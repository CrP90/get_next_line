/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memchr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpetre <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 16:33:31 by cpetre            #+#    #+#             */
/*   Updated: 2017/12/15 16:43:52 by cpetre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const char	*ch;
	size_t		i;

	ch = (const char *)s;
	i = -1;
	while (++i < n)
	{
		if (*(ch + i) == (char)c)
			return ((void *)ch + i);
	}
	return (NULL);
}

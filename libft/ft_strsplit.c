/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpetre <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/17 17:23:22 by cpetre            #+#    #+#             */
/*   Updated: 2017/12/17 17:36:53 by cpetre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	word_len(char const *s, char c)
{
	size_t	i;

	i = 0;
	while (s[i] && s[i] != c)
		i++;
	return (i);
}

static char		*next_word(char const *s, char c)
{
	while (*s && *s == c)
		s++;
	return ((char *)s);
}

static void		cleanup(char **split, size_t current)
{
	while (current-- > 0)
	{
		ft_strdel(&split[current]);
	}
	ft_strdel(split);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**split;
	size_t	current;
	size_t	wordcount;

	if (!s)
		return (NULL);
	wordcount = ft_countwords((char *)s, c);
	split = (char **)ft_memalloc((wordcount + 1) * sizeof(char *));
	if (split == NULL)
		return (NULL);
	current = 0;
	while (current < wordcount)
	{
		s = next_word(s, c);
		split[current] = ft_strsub(s, 0, word_len(s, c));
		if (split[current] == NULL)
		{
			cleanup(split, current);
			return (NULL);
		}
		current++;
		s += word_len(s, c);
	}
	split[wordcount] = NULL;
	return (split);
}

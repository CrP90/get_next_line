/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpetre <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 14:43:44 by cpetre            #+#    #+#             */
/*   Updated: 2018/01/26 14:33:43 by cpetre           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static char		*add_char(char const *s1, char ch)
{
	char			*result;
	size_t			i;
	size_t			len;

	len = ft_strlen(s1);
	result = ft_strnew(len + 1);
	if (!result)
		return (NULL);
	i = 0;
	while (i < len)
	{
		result[i] = s1[i];
		i++;
	}
	result[i] = ch;
	return (result);
}

static int		get_line(char **dst, char *src, char ch)
{
	int				i;
	int				count;

	i = 0;
	count = 0;
	while (src[i])
	{
		if (src[i] == ch)
			break ;
		i++;
	}
	if (!(*dst = ft_strnew(i)))
		return (0);
	while (src[count] && count < i)
	{
		if (!(*dst = add_char(*dst, src[count])))
			return (0);
		count++;
	}
	return (i);
}

static t_list	*get_file(t_list **file, int fd)
{
	t_list			*temp;

	temp = *file;
	while (temp)
	{
		if ((int)temp->content_size == fd)
			return (temp);
		temp = temp->next;
	}
	temp = ft_lstnew("\0", fd);
	ft_lstadd(file, temp);
	temp = *file;
	return (temp);
}

int				get_next_line(const int fd, char **line)
{
	static t_list	*file;
	t_list			*current;
	char			buff[BUFF_SIZE + 1];
	int				i;
	int				count;

	if ((fd < 0 || line == NULL || read(fd, buff, 0) < 0))
		return (-1);
	current = get_file(&file, fd);
	ERRORCHECK((*line = ft_strnew(1)));
	while ((count = read(fd, buff, BUFF_SIZE)))
	{
		buff[count] = '\0';
		ERRORCHECK((current->content = ft_strjoin(current->content, buff)));
		if (ft_strchr(buff, '\n'))
			break ;
	}
	if (count < BUFF_SIZE && !ft_strlen(current->content))
		return (0);
	i = get_line(line, current->content, '\n');
	if (i < (int)ft_strlen(current->content))
		current->content += (i + 1);
	else
		ft_strclr(current->content);
	return (1);
}
